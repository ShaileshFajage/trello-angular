import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BoardComponent} from './board/board.component'
import { InsideBoardComponent } from './inside-board/inside-board.component';


const routes: Routes = [
  {path: '', redirectTo: '/boards', pathMatch: 'full' },
  {path:'boards', component: BoardComponent},
  {path:':id', component: InsideBoardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
