import { Component } from '@angular/core';
import { BoardService } from './board.service';
import {HttpClient} from '@angular/common/http'
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';


interface Board  {
  name : string
}

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
  providers: [NgbModalConfig, NgbModal]

})
export class BoardComponent {

  boards : any
  boardName : string = '' 

  constructor(private boardService : BoardService, private http : HttpClient, config: NgbModalConfig, private modalService: NgbModal){
    config.backdrop = 'static';
		config.keyboard = false;
  }

  ngOnInit(){
    this.boardService.getBoards().subscribe((data)=>{
      console.log(data)  
      // var rows2 = <Array<any>>data;
      this.boards = data
    }, (error)=>{
      console.log(error)
    })
  }

  open(content : any) {
		this.modalService.open(content)
	}

  addBoard(){
    this.boardService.addBoard(this.boardName).subscribe((data)=>{
      console.log(data)
      this.boards.push(data)
    }, (error)=>{
      console.log(error)
    })
  }

}
