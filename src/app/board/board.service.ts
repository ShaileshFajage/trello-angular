import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { key, token } from 'src/key-token';



@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private http : HttpClient) { }

  url : string = `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`

  getBoards(){
    return this.http.get(this.url)
  }

  addBoard(name : string){
    return this.http.post(`https://api.trello.com/1/boards/?name=${name}&key=${key}&token=${token}`, {name:name})
  }

  getBoard(id : string){
    return this.http.get(`https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`)
  }
}
