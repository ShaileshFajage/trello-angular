import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InsideBoardComponent } from './inside-board.component';


describe('InsideBoardComponent', () => {
  let component: InsideBoardComponent;
  let fixture: ComponentFixture<InsideBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsideBoardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InsideBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
