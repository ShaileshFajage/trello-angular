import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Details } from '../types/details'
import { BoardService } from '../board/board.service';

@Component({
  selector: 'app-inside-board',
  templateUrl: './inside-board.component.html',
  styleUrls: ['./inside-board.component.css'],
})
export class InsideBoardComponent {
  id = this.route.snapshot.paramMap.get('id');
  boardDetails : any
  // boardDetails : Details = {
  //   id : this.route.snapshot.paramMap.get('id'),
  //   name : ''
  // }
 
  constructor(private route: ActivatedRoute, private boardService : BoardService) {
    console.log(typeof this.id);
  }

  ngOnInit(){
  if(this.id)
  {
    // this.boardService.getBoard(this.id).subscribe((data)=>{
    //   console.log(data)
    //   this.boardDetails = data
    // })
  }
  }
}
