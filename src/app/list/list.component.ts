import { Component, Input } from '@angular/core';
import { ListService } from './list.service';
import { BoardService } from '../board/board.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  @Input() id : string | null = ''
  lists : any
  boardDetails : any
  listName : string = ''
 

  constructor(private listService : ListService, private boardService : BoardService){}

  ngOnInit() {
    if (this.id) {
      this.listService.getLists(this.id).subscribe((data)=>{
        this.lists = data
        // console.log(this.lists)
      }, (error)=>{
        console.log(error)
      })

      this.boardService.getBoard(this.id).subscribe((data)=>{
        this.boardDetails = data
      })

    }
  }

  addList(){
    if(this.id)
    {
      this.listService.addList(this.id, this.listName).subscribe((data)=>{
        this.lists.push(data)
      })
    }
  }

  deleteList(id : string){
    this.listService.deleteList(id).subscribe((data)=>{
      this.lists = this.lists.filter((list : any)=>{
        return list.id!==id
      })
    }, (error)=>{
      console.log(error)
    })
  }
}
