import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { key, token} from 'src/key-token'; 


@Injectable({
  providedIn: 'root'
})

export class ListService {

  constructor(private httpClient : HttpClient) {
  }

  getLists(id : string){
    return this.httpClient.get(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`)
  }

  addList(id : string, name : string)
  {
    return this.httpClient.post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${key}&token=${token}`, {id:id, name:name})
  }

  deleteList(id : string){
    console.log(id)
    return this.httpClient.post(`https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`, {id:id})
  }

}
